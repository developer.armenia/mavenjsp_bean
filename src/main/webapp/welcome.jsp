<html>
<head>
    <title>Welcome Page</title>
</head>

<jsp:useBean id="person" class="main.java.PersonBean" scope="request" />
<jsp:setProperty property="*" name="person"/>
<body>

            Name of Person is : <jsp:getProperty name="person" property="name" /><br>
            Email of Person is : <jsp:getProperty name="person" property="email" /><br>
            City of Person is : <jsp:getProperty name="person" property="city" /><br>
            Address of Person is : <jsp:getProperty name="person" property="address" /><br>

    </body>
</html>