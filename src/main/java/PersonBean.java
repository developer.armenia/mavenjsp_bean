package main.java;

/**
 * Created by User on 4/21/2019.
 */
import java.io.Serializable;

public class PersonBean implements Serializable
{
    private String name;
    private String email;
    private String city;
    private String address;

    public void setName(String name)
    {
        this.name = name;
    }
    public String getName()
    {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
